﻿namespace FetchContentService.models
{
    public class ContentModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
