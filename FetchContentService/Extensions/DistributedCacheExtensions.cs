﻿using Microsoft.Extensions.Caching.Distributed;
using Serilog;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace FetchContentService.Extensions
{
    public static class DistributedCacheExtensions
    {
        public static async Task SetRecordAsync<T>(this IDistributedCache cache,
        string recordId,
        T data,
        TimeSpan? absoluteExpireTime = null,
        TimeSpan? unusedExpireTime = null)
        {
            try
            {
                var options = new DistributedCacheEntryOptions();

                options.AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromDays(30);
                options.SlidingExpiration = unusedExpireTime;

                Log.Information("Save content");

                var cacheKey = $"{typeof(T).FullName}_{Guid.NewGuid()}";
                var json = JsonSerializer.Serialize(data);

                await cache.SetStringAsync(cacheKey, json, options);

                // Retrieve the list of cache keys for records of type T
                var cacheKeysJson = await cache.GetStringAsync($"{typeof(T).FullName}_keys");
                var cacheKeys = cacheKeysJson is not null ? JsonSerializer.Deserialize<List<string>>(cacheKeysJson) : new List<string>();

                // Add the current cache key to the list
                cacheKeys.Add(cacheKey);

                // Store the updated list of cache keys
                await cache.SetStringAsync($"{typeof(T).FullName}_keys", JsonSerializer.Serialize(cacheKeys));

                Log.Information("Content saved");
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error($"An error occurred while saving the content: {ex.Message}");
                throw;
            }
        }


        public static async Task<T?> GetRecordAsync<T>(this IDistributedCache cache, string recordId)
        {
            try
            {
                Log.Information("Get post");
                var jsonData = await cache.GetStringAsync(recordId);

                if (jsonData is null)
                {
                    Log.Warning("post is empty");
                    return default(T);
                }

                return JsonSerializer.Deserialize<T>(jsonData);
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while getting the record with ID {recordId}");
                return default(T);
            }
        }

        public static async Task<List<T>> GetRecordsAsync<T>(this IDistributedCache cache)
        {
            try
            {
                Log.Information("Getting records from cache");
                var cacheKeysJson = await cache.GetStringAsync(typeof(T).FullName + "_keys");

                if (cacheKeysJson is null)
                {
                    Log.Warning("cache keys are empty");
                    return new List<T>();
                }

                var cacheKeys = JsonSerializer.Deserialize<List<string>>(cacheKeysJson);
                var records = new List<T>();

                foreach (var cacheKey in cacheKeys)
                {
                    var recordJson = await cache.GetStringAsync(cacheKey);

                    if (recordJson is not null)
                    {
                        var record = JsonSerializer.Deserialize<T>(recordJson);
                        records.Add(record);
                    }
                }

                Log.Information("Return records");
                return records;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while getting the records of type {typeof(T).FullName}");
                return new List<T>();
            }
        }


        public static async Task ModifyRecordsAsync<T>(this IDistributedCache cache, Func<T, bool> filterFunc, Action<T> modifyAction)
        {
            try
            {
                Log.Information("Modifying records in cache");
                var cacheKeysJson = await cache.GetStringAsync(typeof(T).FullName + "_keys");

                if (cacheKeysJson is null)
                {
                    Log.Warning("Cache keys are empty");
                    return;
                }

                var cacheKeys = JsonSerializer.Deserialize<List<string>>(cacheKeysJson);

                foreach (var cacheKey in cacheKeys)
                {
                    var recordJson = await cache.GetStringAsync(cacheKey);

                    if (recordJson is not null)
                    {
                        var record = JsonSerializer.Deserialize<T>(recordJson);

                        if (filterFunc(record))
                        {
                            modifyAction(record);
                            var modifiedJson = JsonSerializer.Serialize(record);
                            await cache.SetStringAsync(cacheKey, modifiedJson);
                        }
                    }
                }

                Log.Information("Records modified in cache");
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while modifying records of type {typeof(T).FullName}");
                throw;
            }
        }

    }
}
