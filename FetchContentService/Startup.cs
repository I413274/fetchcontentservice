﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;
using MediatR;
using MassTransit;
using FetchContentService.EventHandler;
using FetchContentService.AsyncDataServices;
using FetchContentService.EventProcessing;
using FetchContentService.data;
using Microsoft.EntityFrameworkCore;
using FetchContentService.repository;
using StackExchange.Redis;
using Microsoft.AspNetCore.Connections;
using RabbitMQ.Client;

public class Startup
{
    public IConfiguration Configuration { get; }

    private readonly IWebHostEnvironment _env;

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        Configuration = configuration;
        _env = env;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        services.AddHostedService<MessageBusSubscriber>();
        services.AddHostedService<MessageBusSubscriberGDPR>();
        services.AddSingleton<IEventProcessor, EventProcessor>();
        services.AddSingleton(sp =>
        {
            var configuration = sp.GetRequiredService<IConfiguration>();
            var factory = new ConnectionFactory()
            {
                HostName = configuration["RabbitMQHost"],
                Port = int.Parse(configuration["RabbitMQPort"])
            };
            return factory;
        });
        //services.AddDbContext<DbContextClass>(opt =>
        //opt.UseInMemoryDatabase("InMemory"));
        services.AddStackExchangeRedisCache(options =>
        {
            options.Configuration = Configuration.GetConnectionString("Redis");
            options.InstanceName = "fetch";
        });

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "FetchContentService", Version = "v1" });
        });
        services.AddCors(c =>
        {
            c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend v1"));
        }

        app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}