﻿using Microsoft.EntityFrameworkCore;
using FetchContentService.models;

namespace FetchContentService.data
{
    public class DbContextClass : DbContext
    {

        public DbContextClass(DbContextOptions<DbContextClass> options) : base(options)
        {
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{
        //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        //}

        public DbSet<ContentModel> ContentModels { get; set; }
    }
}
