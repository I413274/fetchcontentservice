﻿using FetchContentService.data;
using FetchContentService.models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Numerics;
using Serilog;

namespace FetchContentService.repository
{
    public class ContentRepository 
    {
        private readonly DbContextClass _dbContext;

        public ContentRepository(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ContentModel> AddContentAsync(ContentModel model)
        {
            try
            {
                Log.Information("Adding content to the database");
                var result = _dbContext.ContentModels.Add(model);
                await _dbContext.SaveChangesAsync();
                Log.Information("Content added successfully");
                return result.Entity;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while adding content to the database");

                return null;
            }
        }


        public async Task<int> DeleteContentAsync(int id)
        {
            try
            {
                Log.Information($"Deleting content");

                var filteredData = _dbContext.ContentModels.FirstOrDefault(x => x.Id == id);

                if (filteredData != null)
                {
                    _dbContext.ContentModels.Remove(filteredData);
                    int rowsAffected = await _dbContext.SaveChangesAsync();

                    Log.Information($"Content deleted successfully");

                    return rowsAffected;
                }

                Log.Warning($"Content with Id not found");

                return 0;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while deleting content");

                return 0;
            }
        }


        public async Task<ContentModel> GetContentByIdAsync(int id)
        {
            try
            {
                Log.Information($"Retrieving content");

                var content = await _dbContext.ContentModels.FirstOrDefaultAsync(x => x.Id == id);

                if (content != null)
                {
                    Log.Information($"Content retrieved successfully");
                }
                else
                {
                    Log.Warning($"Content not found");
                }

                return content;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while retrieving content");

                return null;
            }
        }


        public async Task<List<ContentModel>> GetContentListAsync()
        {
            try
            {
                Log.Information("Retrieving list of content");

                var contentList = await _dbContext.ContentModels.ToListAsync();

                Log.Information($"Retrieved {contentList.Count} content records");

                return contentList;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while retrieving content list");

                return new List<ContentModel>();
            }
        }


        public async Task<int> UpdateContentAsync(ContentModel contentDetails)
        {
            try
            {
                Log.Information($"Updating content");

                _dbContext.ContentModels.Update(contentDetails);
                int rowsAffected = await _dbContext.SaveChangesAsync();

                Log.Information($"Content updated successfully");

                return rowsAffected;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, $"An error occurred while updating content");

                return 0;
            }
        }

    }
}
