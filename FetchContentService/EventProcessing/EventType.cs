﻿namespace FetchContentService.EventProcessing
{
    enum EventType
    {
        ContentPublished,
        Undetermined
    }
}