﻿using FetchContentService.Events;
using FetchContentService.Extensions;
using FetchContentService.models;
using FetchContentService.repository;
using Microsoft.Extensions.Caching.Distributed;
using ProfileService.Models;
using Serilog;
using System.Text.Json;

namespace FetchContentService.EventProcessing
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IDistributedCache _cache;

        public EventProcessor(IDistributedCache cache)
        {
            _cache = cache;
        }

        public void ProcessEvent(string message)
        {
            var eventType = DetermineEvent(message);

            switch (eventType)
            {
                case EventType.ContentPublished:
                    addContent(message);
                    break;
                default:
                    break;
            }
        }

        private EventType DetermineEvent(string notificationMessage)
        {
            Log.Information("Determining Event");

            var eventType = JsonSerializer.Deserialize<GenericEventModel>(notificationMessage);
            Log.Information(eventType.Event);
            switch(eventType.Event) 
            {
                case "Content_Published":
                    Log.Information("Content published Event Detected");
                    return EventType.ContentPublished;
                default:
                    Log.Warning("Could not determine event type");
                    return EventType.Undetermined;
            }
        }

        private void addContent(string ContentPublishedMessage)
        {
            var contentPublishedDto = JsonSerializer.Deserialize<ContentModel>(ContentPublishedMessage);

            try
            {
                if (contentPublishedDto != null)
                {
                    Log.Information("Save content to cache");
                    _cache.SetRecordAsync(Convert.ToString(contentPublishedDto.Id), contentPublishedDto);                    
                }
                else
                {
                    Log.Warning("content already exists");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Could not add content to DB");
            }
        }        
    }
}