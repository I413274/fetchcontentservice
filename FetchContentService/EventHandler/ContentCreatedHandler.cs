﻿using FetchContentService.Events;
using MassTransit;
using MediatR;

namespace FetchContentService.EventHandler
{
    public class ContentCreatedEventHandler : IConsumer<ContentCreatedEvent>
    {
        public Task Consume(ConsumeContext<ContentCreatedEvent> context)
        {
            // Extract the data from the event and perform necessary actions
            var id = context.Message.Id;
            var title = context.Message.Title;
            var description = context.Message.Description;

            // Handle the event and process the data as needed

            return Task.CompletedTask;
        }
    }
}
