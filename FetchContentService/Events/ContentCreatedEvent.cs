﻿using MediatR;

namespace FetchContentService.Events
{
    public class ContentCreatedEvent : INotification
    {
        public ContentCreatedEvent(string userID, string userName, string title, string description)
        {
            UserID = userID;
            UserName = userName;
            Title = title;
            Description = description;
        }

        public int Id { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
