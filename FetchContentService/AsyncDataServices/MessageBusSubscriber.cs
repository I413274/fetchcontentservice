﻿using FetchContentService.EventProcessing;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;
using System.Text;
using System.Threading.Channels;

namespace FetchContentService.AsyncDataServices
{
    public class MessageBusSubscriber : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IEventProcessor _eventProcessor;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;
        private readonly ConnectionFactory factory;

        public MessageBusSubscriber(IConfiguration configuration, IEventProcessor eventProcessor, ConnectionFactory connectionFactory)
        {
            _configuration = configuration;
            _eventProcessor = eventProcessor;
            factory = connectionFactory;
            InitialiseRabbitMQ();
        }

        private void InitialiseRabbitMQ()
        {
            try
            {
                factory.HostName = _configuration["RabbitMQHost"];
                factory.Port = int.Parse(_configuration["RabbitMQPort"]);

                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: "FetchContent", type: ExchangeType.Topic, true);
                _channel.QueueDeclare("fetchqueue", true, false, false, null);
                _queueName = _channel.QueueDeclare().QueueName;
                _channel.QueueBind(queue: _queueName,
                    exchange: "FetchContent",
                    routingKey: "fyves");

                Log.Information("Listening on the Message Bus");

                _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An error occurred while initializing RabbitMQ");
            }
        }


        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (ModuleHandle, ea) =>
            {
                Console.WriteLine("Event Received");

                var body = ea.Body;
                var notificationMessage = Encoding.UTF8.GetString(body.ToArray());

                _eventProcessor.ProcessEvent(notificationMessage);
            };

            _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            Log.Information("Connection Shutdown");
        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }
        }
    }
}