﻿using FetchContentService.EventProcessing;
using FetchContentService.Extensions;
using FetchContentService.models;
using Microsoft.Extensions.Caching.Distributed;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;
using System.Text;
using System.Text.Json;
using System.Threading.Channels;

namespace FetchContentService.AsyncDataServices
{
    public class MessageBusSubscriberGDPR : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IEventProcessor _eventProcessor;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;
        private readonly IDistributedCache _cache;
        private readonly ConnectionFactory factory;

        public MessageBusSubscriberGDPR(IConfiguration configuration, IEventProcessor eventProcessor, IDistributedCache cache, ConnectionFactory connectionFactory)
        {
            _configuration = configuration;
            _eventProcessor = eventProcessor;
            _cache = cache;
            factory = connectionFactory;
            InitialiseRabbitMQ();
        }

        private void InitialiseRabbitMQ()
        {
            try
            {
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: "purgedata", type: ExchangeType.Fanout, true);
                _channel.QueueDeclare("purge-fetch", true, false, false, null);
                _queueName = _channel.QueueDeclare().QueueName;
                _channel.QueueBind(queue: _queueName,
                    exchange: "purgedata",
                    routingKey: "fyves");

                Log.Information("Listening on the Message Bus");

                _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error($"An error occurred while initializing RabbitMQ: {ex.Message}");
            }
        }


        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                stoppingToken.ThrowIfCancellationRequested();

                var consumer = new EventingBasicConsumer(_channel);

                consumer.Received += (ModuleHandle, ea) =>
                {
                    try
                    {
                        Log.Information("Event Received");
                        Log.Information("Delete account");
                        var body = ea.Body;
                        var notificationMessage = Encoding.UTF8.GetString(body.ToArray());

                        var contentPublishedDto = JsonSerializer.Deserialize<string>(notificationMessage);
                        _cache.ModifyRecordsAsync<ContentModel>(
                            filterFunc: (item) => item.UserID == contentPublishedDto,
                            modifyAction: (item) => item.UserName = "[DELETED]"
                        );
                    }
                    catch (Exception ex)
                    {
                        // Log the exception for troubleshooting
                        Log.Error(ex, "An error occurred while processing the received event");
                    }
                };

                _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An error occurred in the ExecuteAsync method");
            }

            return Task.CompletedTask;
        }


        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            Log.Information("Connection Shutdown");
        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }
        }
    }
}