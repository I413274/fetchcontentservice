﻿using FetchContentService.Events;
using FetchContentService.Extensions;
using FetchContentService.models;
using FetchContentService.repository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Serilog;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FetchContentService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FetchContentController : ControllerBase
    {

        private readonly IDistributedCache _cache;

        public FetchContentController(IDistributedCache cache)
        {
            _cache = cache;
        }

        // GET api/<FetchController>/5
        [HttpGet]
        public async Task<ContentModel> Get(string id)
        {
            try
            {
                Console.WriteLine(id);
                ContentModel model = await _cache.GetRecordAsync<ContentModel>(id);
                Console.WriteLine(model);
                if (model == null)
                {
                    Log.Information("model is empty");
                    return null;
                }
                return model;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error($"An error occurred while getting the model with ID {id}: {ex.Message}");
                throw;
                // Optionally, perform error handling or rethrow the exception
                // throw;
            }
        }

        [HttpGet]
        [Route("Posts")]
        public async Task<List<ContentModel>> getModels()
        {
            try
            {
                Log.Information("Get list of posts");
                List<ContentModel> models = await _cache.GetRecordsAsync<ContentModel>();
                Log.Information($"Post count: {models.Count}");
                return models;
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error($"An error occurred while getting the list of models: {ex.Message}");
                throw;
                // Optionally, perform error handling or rethrow the exception
                // throw;
            }
        }


        // POST api/<FetchController>
        [HttpPost]
        public async Task<ContentModel> AddContentAsync(ContentModel content)
        {
            try 
            {
                Log.Information("Save content in cache");
                await _cache.SetRecordAsync("post", content);                
                return content;
            }
            catch(Exception ex)
            {
                Log.Information("not saved");
                throw;
            }
        }

        // PUT api/<FetchController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        //
        //
        // api/<FetchController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
